**PATRONES DE DISENO**

**Adapter**
Este nos permite convertir la interfaz de una clase en otra que es la que esperan los clientes. 
Permite que trabajen juntas clases que de otro modo no podrían
por tener interfaces incompatibles. También es conocido como Wrapper

**Fatory Method**
 Este nos permite la creación de objetos de un subtipo determinado a través de una clase Factory.
 Esto es especialmente útil cuando no sabemos, en tiempo de diseño, el subtipo que vamos a utilizar
 o cuando queremos delegar la lógica de creación de los objetos a una clase Factory.
 Utilizando este patrón podemos crear instancias dinámicamente mediante la configuración
 estableciendo cual será la implementación a utilizar en un archivo de texto, XML
 properties o mediante cualquier otra estrategia.
 
**Singleton**
El patrón de diseño Singleton (soltero) recibe su nombre debido a que sólo se puede tener una única
instancia para toda la aplicación de una determinada clase, esto se logra restringiendo la libre 
creación de instancias de esta clase mediante el operador new e imponiendo un
constructor privado y un método estático para poder obtener la instancia.
 
 **Prototype**
 Este ess un patrón de diseño creacional, que tiene como objetivo crear a partir de un modelo.
 Especificar el tipo de objetos que se crearán mediante una instancia prototípica, y crear nuevos objetos copiando este prototipo.
 Tiene gran simplicidad ya que su concepto es de hacer una copia exacta de otro objeto
 esto en lugar de crear uno nuevo, permite crear los objetos pre diseñados sin la necesidad de conocer los detalles de la creación.
 
 **Proxy**
 Este es el que nos permite hacer una gran cantidad de cosas sin que el usuario final se de cuenta de lo que esta pasando.
 Podríamos definir a un Proxy como una clase o componentes que hace el papel de intermediario entre la clase que queremos 
 utilizar el cliente que la esta utilizando.
 
** Para el proyecto Final**
 Utilizaremos el MVC, ya que es el que mejor se adaptata al trabajo que realizaremos, ya que debido a que utlizaremos clases para intermediar
 las interfazes y es mas u til ya que el modelo quedaria intacto y el controlador o intermediario, quedaria a disposicion para la modificacion y envio
 del backend del proyecto, el front-end solo se encargara de alterar el view y las rutas.Queremos en particular probar otros tipos de patrones, para verificar cual se nos apega a 
 necesidad.
 
 
 fuente:
- https://www.arquitecturajava.com/usando-el-patron-factory/
 -arantxa.ii.uam.es/~eguerra/docencia/0708/04%20Creacion.pdf
